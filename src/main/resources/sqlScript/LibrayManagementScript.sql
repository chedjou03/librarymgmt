DROP SCHEMA IF EXISTS libraryManagementSystem;
CREATE SCHEMA libraryManagementSystem;
USE libraryManagementSystem;


-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS users (
    username varchar(50) not null,
    password varchar(120) not null,
    enabled boolean not null,
    PRIMARY KEY (`username`)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS authorities (
    username varchar(50) not null,
    role varchar(50) not null,
    CONSTRAINT fk_username_in_authorities FOREIGN KEY (username) REFERENCES users (username) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE=InnoDB  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS category (
  category_id int(10) NOT NULL AUTO_INCREMENT,
  category_name varchar(50) NOT NULL,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS publisher (
  publisher_id int(10) NOT NULL AUTO_INCREMENT,
  publisher_name varchar(50) NOT NULL,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publisher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS author (
  author_id int(10) NOT NULL AUTO_INCREMENT,
  author_name varchar(50) NOT NULL,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS book (
  book_id int(10) NOT NULL AUTO_INCREMENT,
  ISBN varchar(100) NOT NULL,
  title varchar(100) NOT NULL,
  author varchar(100) NOT NULL,
  category_id int(10),
  publisher_id int(10),
  author_id int(10),
  publisher varchar(100) NOT NULL,
  quantity int(10) NOT NULL,
  added_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`book_id`),
  CONSTRAINT `fk_category_id_in_book` FOREIGN KEY (category_id) REFERENCES category (category_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_publisher_id_in_book` FOREIGN KEY (publisher_id) REFERENCES publisher (publisher_id) ON DELETE RESTRICT ON UPDATE CASCADE,
   CONSTRAINT `fk_author_id_in_book` FOREIGN KEY (author_id) REFERENCES author (author_id) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;




CREATE TABLE IF NOT EXISTS  address (
  address_id int(10) NOT NULL AUTO_INCREMENT,
  address VARCHAR(50) NOT NULL,
  address2 VARCHAR(50) DEFAULT NULL,
  state VARCHAR(20) NOT NULL,
  city VARCHAR(20) NOT NULL,
  country VARCHAR(20) NOT NULL,
  postal_code VARCHAR(10) DEFAULT NULL,
  phone VARCHAR(20) NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (address_id)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;





CREATE TABLE IF NOT EXISTS employee (
  employee_id int(10) NOT NULL AUTO_INCREMENT,
  first_name varchar(100) NOT NULL,
  middle_name varchar(100) NOT NULL,
  last_name varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  address_id int(10) NOT NULL ,
  username varchar(50),
  added_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`employee_id`),
  CONSTRAINT `fk_addressId_in_employee` FOREIGN KEY (address_id) REFERENCES address (address_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT  `fk_username_in_employee`  FOREIGN KEY (username) REFERENCES users (username) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;



CREATE TABLE IF NOT EXISTS student (
  student_id int(10) NOT NULL AUTO_INCREMENT,
  first_name varchar(100) NOT NULL,
  middle_name varchar(100) NOT NULL,
  last_name varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  address_id int(10) NOT   NULL,
  username varchar(50),
  added_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  last_update  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`student_id`),
  CONSTRAINT `fk_username_in_student` FOREIGN KEY (username) REFERENCES users (username) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_addressId_in_student` FOREIGN KEY (address_id) REFERENCES address (address_id) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;



CREATE TABLE IF NOT EXISTS  student_book (
  student_id int(10) NOT NULL,
  book_id int(10) NOT NULL,
  booked_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  scheduled_return_date TIMESTAMP,
  return_date  TIMESTAMP,
  PRIMARY KEY  (student_id,book_id),
  CONSTRAINT fk_studentd_in_studentbook FOREIGN KEY (student_id) REFERENCES student (student_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT fk_bookId_in_studentbook FOREIGN KEY (book_id) REFERENCES book (book_id) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;


commit;


INSERT INTO `users` 
VALUES 
('student','$2a$10$A5BjRKbtY6bQb2vYZUxeYuCL3zAStVaV8E7CtgxTxudpP5cZB/fdW',1),
('librarian','$2a$10$A5BjRKbtY6bQb2vYZUxeYuCL3zAStVaV8E7CtgxTxudpP5cZB/fdW',1),
('admin','$2a$10$A5BjRKbtY6bQb2vYZUxeYuCL3zAStVaV8E7CtgxTxudpP5cZB/fdW',1);



INSERT INTO `authorities` 
VALUES 
('student','ROLE_STUDENT'),
('librarian','ROLE_LIBRARIAN'),
('admin','ROLE_ADMIN');

commit;

INSERT INTO `libraryManagementSystem`.`address`
(`address`,
`address2`,
`state`,
`city`,
`country`,
`postal_code`,
`phone`,
`last_update`)
VALUES
(
'1200 Robley Dr',
'',
'Louisiana',
'Lafayette',
'United State',
'70503',
'501-472-7137',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`address`
(`address`,
`address2`,
`state`,
`city`,
`country`,
`postal_code`,
`phone`,
`last_update`)
VALUES
(
'3550 Pleasant Hill Rd.',
'',
'Georgia',
'Duluth',
'United State',
'30966',
'501-472-7137',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`address`
(`address`,
`address2`,
`state`,
`city`,
`country`,
`postal_code`,
`phone`,
`last_update`)
VALUES
(
'10 Brier spring Rd.',
'',
'Arkansas',
'Conway',
'United State',
'72034',
'501-472-3457',
CURRENT_TIMESTAMP);

commit;

INSERT INTO `libraryManagementSystem`.`employee`
(`first_name`,
`middle_name`,
`last_name`,
`email`,
`address_id`,
`username`,
`added_date`,
`last_update`)
VALUES
('Simplice',
'Gildasse',
'Chedjou',
'chedjou03@yahoo.fr',
4,
'admin',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`employee`
(`first_name`,
`middle_name`,
`last_name`,
`email`,
`address_id`,
`username`,
`added_date`,
`last_update`)
VALUES
('James',
'Saint',
'Patrick',
'jamesStpatric@yahoo.com',
5,
'librarian',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP);

commit;

INSERT INTO `libraryManagementSystem`.`student`
(`first_name`,
`middle_name`,
`last_name`,
`email`,
`address_id`,
`username`,
`added_date`,
`last_update`)
VALUES
('Angela',
'Maria',
'Valdez',
'angelavaldez@yahoo.com',
6,
'student',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP);

commit;

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Drama', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Crime', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('History', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Memoire', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Science', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Poetry	Review', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Political', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Spirituality', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Prayer', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Review', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Fiction', CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Romance', CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`category`
(`category_name`,`last_update`)
VALUES
('Suspense	', CURRENT_TIMESTAMP);

commit;

INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('George W Bush',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Barrack H Obama',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Hillary H Clinton',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('James B Comey',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Samuel Etoo Fils',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Thierry Gilardy',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Bill Oreilly',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
(' Condolizza Rice',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Rachel M Maddow',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Nicolas Sarkozy',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('Michael Bernet Wolf ',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`author`
(
`author_name`,
`last_update`)
VALUES
('William Jefferson Clinton ',
CURRENT_TIMESTAMP);



commit;

INSERT INTO `libraryManagementSystem`.`publisher`
(`publisher_name`,`last_update`)
VALUES
('Broadway Books',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`publisher`
(`publisher_name`,`last_update`)
VALUES
('Simon & Schuster',
CURRENT_TIMESTAMP);

INSERT INTO `libraryManagementSystem`.`publisher`
(`publisher_name`,`last_update`)
VALUES
('Macmillan Publishers',
CURRENT_TIMESTAMP);


INSERT INTO `libraryManagementSystem`.`publisher`
(`publisher_name`,`last_update`)
VALUES
('Crown/Three Rivers Press',
CURRENT_TIMESTAMP);


commit;



ALTER TABLE `libraryManagementSystem`.`book`
DROP COLUMN author;

ALTER TABLE `libraryManagementSystem`.`book`
DROP COLUMN publisher;

commit;

ALTER TABLE `libraryManagementSystem`.`author`
ADD COLUMN nbr_books Int(10);

ALTER TABLE `libraryManagementSystem`.`publisher`
ADD COLUMN nbr_books Int(10);

ALTER TABLE `libraryManagementSystem`.`category`
ADD COLUMN nbr_books Int(10);


ALTER TABLE `libraryManagementSystem`.`book`
ADD COLUMN nbr_pages Int(10);


ALTER TABLE `libraryManagementSystem`.`book`
ADD COLUMN language varchar(50);


ALTER TABLE `libraryManagementSystem`.`book`
ADD COLUMN publication_date TIMESTAMP;


ALTER TABLE `libraryManagementSystem`.`book`
ADD COLUMN covert BLOB;

commit;


INSERT INTO `libraryManagementSystem`.`book`
(
`ISBN`,
`title`,
`category_id`,
`publisher_id`,
`author_id`,
`quantity`,
`added_date`,
`last_update`)
VALUES
(
'9780307590619',
'Decision Points',
7,
4,
4,
20,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP);

commit;


INSERT INTO `libraryManagementSystem`.`address`
(`address`,
`address2`,
`state`,
`city`,
`country`,
`postal_code`,
`phone`,
`last_update`)
VALUES
(
'700 CajunDome Blvd.',
'',
'Louisiana',
'Lafayette',
'United State',
'30966',
'337-453-3421',
CURRENT_TIMESTAMP);


INSERT INTO `users` 
VALUES 
('librarian2','$2a$10$A5BjRKbtY6bQb2vYZUxeYuCL3zAStVaV8E7CtgxTxudpP5cZB/fdW',1);



INSERT INTO `authorities` 
VALUES 
('librarian2','ROLE_LIBRARIAN');


INSERT INTO `libraryManagementSystem`.`employee`
(`first_name`,
`middle_name`,
`last_name`,
`email`,
`address_id`,
`username`,
`added_date`,
`last_update`)
VALUES
('Erin',
'Bethernadet',
'Bernett',
'Erin@gmail.com',
7,
'librarian2',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP);

commit;

















