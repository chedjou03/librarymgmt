$( document ).ready(function() {
	console.log("User Books page ready!" );
	var apiContextPath = $("#ApiContextPathId").val();
	var restApiLibariansUrl = apiContextPath+'/librarians';
	var librarianTable;
   

    populateLibrarianDataTable();
    
    
    function populateLibrarianDataTable(){
    	var librarians = getLibrarians();
        librarianTable = $('#librarianTableId').DataTable({
    		data:librarians,
    		columns:[
    			{
                    "class":          "details-control",
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ""
                },
    			{'data':'id'},
    			{'data':'firstName'},
    			{'data':'middleName'},
    			{'data':'lastName'},
    			{'data':'email'},
    			{
    	            "targets": -1,
    	            "data": null,
    	            "defaultContent": "<button>Edit</button>"
    	        }
    		],
        	"scrollY":        "300px",
            "scrollCollapse": true,
            "paging":         true
        });
    }
    
    
    function getLibrarians(){
    	return $.ajax({
    		type : 'GET',
        	url : restApiLibariansUrl,
    	    async: false,
    	    error: function (XMLHttpRequest, textStatus, errorThrown) {
    	      	toastr.error('error Loading librarian','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	    },
    	    success: function (librians) {
    	        toastr.info('librarians Loaded','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	    }
    	}).responseJSON;
    }
    
  
    $('#librarianTableId tbody').on('click', 'tr button', function () {
    	var tr = $(this).closest('tr');
    	var row = librarianTable.row( tr );
     	var aLibrarian = row.data();
        setLibrarianModalInfoField(aLibrarian);
        $("#librarianInfoModalId").modal();
    } );
    
    var detailRows = [];
    
    $('#librarianTableId tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = librarianTable.row( tr );
        var data1 = row.data();
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    librarianTable.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
    
    function format ( d ) {
    	 return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; First name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.firstName+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Middle Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.middleName+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.lastName+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.email+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; userName &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.userName+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
			             '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Role &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			             '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.role+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
			         '</tr>'+
			         '<tr>'+
				         '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
				         '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.address.phone+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
				     '</tr>'+
				     '<tr>'+
					     '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
					     '<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.address.address+','+d.address.city+', '+d.address.state+', '+d.address.postalCode+', '+d.address.country+'</td>'+
				     '</tr>'+
			     '</table>';
    }
    
    function setLibrarianModalInfoField(aLibrarian){
    	console.log(aLibrarian);
    	$('#modalLibrarianFirstNameId').val(aLibrarian.firstName);
    	$('#modalLibrarianMiddleNameId').val(aLibrarian.middleName);
    	$('#modalLibrarianLastNameId').val(aLibrarian.lastName);
    	$('#modalLibrarianEmailId').val(aLibrarian.email);
    }
    
});
