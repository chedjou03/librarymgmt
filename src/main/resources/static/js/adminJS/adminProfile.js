var originalState = "";
var theEmployee;
$( document ).ready(function() {
    console.log( "admin profile page ready!" );
    var userName = $("#userNameId").val();
    var userRole = $("#userRoleId").val();
    var apiContextPath = $("#ApiContextPathId").val();
    var restApiEmployeeUrl = apiContextPath+'/employees/';
    var restApiStatesUrl = apiContextPath+'/states';
    
    //cache all input DOM
    var $firstNameInput = $("#firstNameId");
    var $middleNameInput = $("#middleNameId");
    var $lastNameInput = $("#lastNameId");
    var $emailInput = $("#emailId");
    var $addressInput = $("#addressId");
    var $address2Input = $("#address2Id");
    var $cityInput = $("#cityId");
    var $stateInput = $("#stateId");
    var $countryInput = $("#countryId");
    var $phoneNumberInput = $("#phoneNumberId");
    var $zipCodeInput = $("#zipCodeId");
    var $userNameInput = $("#userNameInputId");
    var $password = $("#passwordId");
    
    //cache all Modal input DOM
    var $modalFirstNameInput = $("#modalFirstNameId");
    var $modalMiddleNameInput = $("#modalMiddleNameId");
    var $modalLastNameInput = $("#modalLastNameId");
    var $modalEmailInput = $("#modalEmailId");
    var $modalAddressInput = $("#modalAddressId");
    var $modalAddress2Input = $("#modalAddress2Id");
    var $modalCityInput = $("#modalCityId");
    var $modalStateInput = $("#modalStateId");
    var $modalCountryInput = $("#modalCountryId");
    var $modalPhoneNumberInput = $("#modalPhoneNumberId");
    var $modalZipCodeInput = $("#modalZipCodeId");
    var $modalStateDropdown = $("#modalStateDropdownId");
    
    initializePageAndGetEmployee();
    
    
    function initializePageAndGetEmployee(){
    	$.ajax({
        	type : 'GET',
        	url : restApiEmployeeUrl+userName,
        	success: function  (employee){
        		theEmployee = employee;
        		setAllBasicInfoFields(employee);
        		setAllAddressFields(employee);
        		setAllUserNamePassword(employee);
        		disableBasicInfoFiels();
        		disableAddressFiels();
        		toastr.info(userName+' Profile infos loaded','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
        		},
        	error : function(){
        		disableEditButtons();
        		disableBasicInfoFiels();
        		disableAddressFiels();
        		toastr.error('Error loading '+userName+' profile ', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
        		
        	}  	
        });
    }
    
    $("#editBasicInfoBtnId").click(function() { 
    	setAllModalBasicInfoFields(theEmployee);
    	$("#basicInfoModalId").modal();
    });
    
    $("#editAddressBtnId").click(function() { 
    	loadStates();
    	setAllModalAddressFields(theEmployee);
    	$modalCountryInput.prop('disabled', true);
    	$("#addressModalId").modal();
    });
    
    
    $("#saveBasicInfoModalBtnId").click(function (){
    	var currentFirstName = $modalFirstNameInput.val();
    	var currentLastName = $modalLastNameInput.val();
    	var currentEmail = $modalEmailInput.val();
    	if (!currentFirstName.trim()) {
        	toastr.error('First Name can not be empty', '', {timeOut: 5000, closeButton: true,positionClass: "toast-top-right"});
    	}else if (!currentLastName.trim()) {
        	toastr.error('Last Name can not be empty', '', {timeOut: 5000, closeButton: true,positionClass: "toast-top-right"});
    	}else if (!currentEmail.trim()) {
        	toastr.error('Email can not be empty', '', {timeOut: 5000, closeButton: true,positionClass: "toast-top-right"});
    	}else if(!isEmailAddressValid(currentEmail)){
        	toastr.error('Email not valid', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else{
    		saveEmployeeBasicInfo();
        	$("#basicInfoModalId").modal('hide');
    	}
    	
    });
    
    $("#saveAddressModalBtnId").click(function (){
    	var currentAddress = $modalAddressInput.val();
    	var currentCity = $modalCityInput.val();
    	var currentPhoneNumber = $modalPhoneNumberInput.val();
    	var currentZipCode = $modalZipCodeInput.val();
    	if (!currentAddress.trim()) {
        	toastr.error('Address can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else if (!currentCity.trim()) {
        	toastr.error('City can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else if (!currentPhoneNumber.trim()) {
        	toastr.error('Phone Number not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else if(!isPhoneNumberValid(currentPhoneNumber)){
        	toastr.error('Phone Number format must be 999-999-9999', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else if (!currentZipCode.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.error('Zip Code can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	} else if(!isZipcodeValid(currentZipCode)){
        	toastr.error('Zip code format must be 99999-9999', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else{
    		saveEmployeeAddress();
        	$("#addressModalId").modal('hide');
    	}
    	
    });
    
    
    function saveEmployeeAddress(){
    	theEmployee.address.address = $modalAddressInput.val();
    	theEmployee.address.address2 = $modalAddress2Input.val();
    	theEmployee.address.city = $modalCityInput.val();
    	theEmployee.address.phone = $modalPhoneNumberInput.val();
    	theEmployee.address.postalCode = $modalZipCodeInput.val();
    	theEmployee.address.state  = $modalStateDropdown.val();
    	$.ajax({
    		type : 'PUT',
    		url :restApiEmployeeUrl,
    		data : JSON.stringify(theEmployee),
    		contentType: "application/json",
            dataType: "json",
    		success : function(newEmployee){
    			setAllAddressFields(newEmployee);
    			toastr.success('Address updated','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    		},
    		error : function(){
    			toastr.error('Error updating Address', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    		}
    	});
    }
    
    function saveEmployeeBasicInfo(){
    	theEmployee.firstName = $modalFirstNameInput.val();
    	theEmployee.middleName = $modalMiddleNameInput.val();
    	theEmployee.lastName = $modalLastNameInput.val();
    	theEmployee.email = $modalEmailInput.val();		
    	$.ajax({
    		type : 'PUT',
    		url :restApiEmployeeUrl,
    		data : JSON.stringify(theEmployee),
    		contentType: "application/json",
            dataType: "json",
    		success : function(newEmployee){
    			setAllBasicInfoFields(newEmployee);
    			toastr.success('Basic info updated','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    		},
    		error : function(){
    			toastr.error('Error Basic info', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    		}
    		
    	});
    }
    
    function loadStates(){
    	$.ajax({
        	type : 'GET',
        	url : restApiStatesUrl,
        	success: function  (states){
        		populateStateDropdown(states);
        	},
        	error : function(){
        		console.log("error getting states profile")
        		toastr.error('Error loading states', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
        	}  	
        });
    	
    }
    
    function setAllBasicInfoFields(employee){
    	$firstNameInput.val(employee.firstName);
    	$middleNameInput.val(employee.middleName);
    	$lastNameInput.val(employee.lastName);
    	$emailInput.val(employee.email);
    	$modalFirstNameInput.val(employee.firstName);
    	$modalMiddleNameInput.val(employee.middleName);
    	$modalLastNameInput.val(employee.lastName);
    	$modalEmailInput.val(employee.email);
    }
    
    function setAllModalBasicInfoFields (employee){
    	$modalFirstNameInput.val(employee.firstName);
    	$modalMiddleNameInput.val(employee.middleName);
    	$modalLastNameInput.val(employee.lastName);
    	$modalEmailInput.val(employee.email);
    	$modalFirstNameInput.css({'background-color' : '#fff'});
    	$modalLastNameInput.css({'background-color' : '#fff'});
    	$modalEmailInput.css({'background-color' : '#fff'});
    }
    
    function setAllAddressFields(employee){
    	$addressInput.val(employee.address.address);
    	$address2Input.val(employee.address.address2);
    	$cityInput.val(employee.address.city);
    	$stateInput.val(employee.address.state);
    	$countryInput.val(employee.address.country);
    	$phoneNumberInput.val(employee.address.phone);
    	$zipCodeInput.val(employee.address.postalCode);
    	$modalAddressInput.val(employee.address.address);
    	$modalAddress2Input.val(employee.address.address2);
    	$modalCityInput.val(employee.address.city);
    	$modalStateInput.val(employee.address.state);
    	$modalCountryInput.val(employee.address.country);
    	$modalPhoneNumberInput.val(employee.address.phone);
    	$modalZipCodeInput.val(employee.address.postalCode);
    	originalState = employee.address.state;
    	    
    }
    
    function setAllModalAddressFields(employee){
    	$modalAddressInput.val(employee.address.address);
    	$modalAddress2Input.val(employee.address.address2);
    	$modalCityInput.val(employee.address.city);
    	$modalStateInput.val(employee.address.state);
    	$modalCountryInput.val(employee.address.country);
    	$modalPhoneNumberInput.val(employee.address.phone);
    	$modalZipCodeInput.val(employee.address.postalCode);
    	originalState = employee.address.state;
    	$modalAddressInput.css({'background-color' : '#fff'});
    	$modalCityInput.css({'background-color' : '#fff'});
    	$modalPhoneNumberInput.css({'background-color' : '#fff'});
    	$modalZipCodeInput.css({'background-color' : '#fff'});
    }
    
    function setAllUserNamePassword(employee){
    	$userNameInput.prop('disabled', true);
    	$password.prop('disabled', true);
    	$("#editUserNamePasswordBtnId").prop('disabled', true);
    }
    
    function disableBasicInfoFiels(){
    	$firstNameInput.prop('disabled', true);
    	$middleNameInput.prop('disabled', true);
    	$lastNameInput.prop('disabled', true);
    	$emailInput.prop('disabled', true);
    }
    
    function disableAddressFiels(){
    	$addressInput.prop('disabled', true);
    	$address2Input.prop('disabled', true);
    	$cityInput.prop('disabled', true);
    	$stateInput.prop('disabled', true);
    	$countryInput.prop('disabled', true);
    	$phoneNumberInput.prop('disabled', true);
    	$zipCodeInput.prop('disabled', true);
    }
    
    function disableEditButtons(){
    	$("#editAddressBtnId").prop('disabled', true);
    	$("#editBasicInfoBtnId").prop('disabled', true);
    }
    
    function populateStateDropdown(states){
    	$modalStateDropdown.html('');
    	if(states != '')
        {    
    		$modalStateDropdown.append('<option value="'+originalState+'">'+originalState+'</option>');
            $.each(states, function(i, state) {
            	if(originalState != state.name){
            		$modalStateDropdown.append('<option value="' +state.name+ '">' + state.name + '</option>');
            	}
            });
        }
    }
    
    $('#modalStateDropdownId').change(function () {
        var selectedText = $(this).find("option:selected").text();
    });
    
    $modalFirstNameInput.focusout(function(){
    	var currentFirstName = $modalFirstNameInput.val();
    	if (!currentFirstName.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('First Name can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    	
    });
    
    $modalFirstNameInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalLastNameInput.focusout(function(){
    	var currentlastName = $modalLastNameInput.val();
    	if (!currentlastName.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Last Name can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    	
    });
    
    $modalLastNameInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalEmailInput.focusout(function(){
    	var currentEmail = $modalEmailInput.val();
    	if (!currentEmail.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Email can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}else if(!isEmailAddressValid(currentEmail)){
    		$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Email not valid', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    });
    
    $modalEmailInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalAddressInput.focusout(function(){
    	var currentAddress = $modalAddressInput.val();
    	if (!currentAddress.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('address can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    });
    
    $modalEmailInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalCityInput.focusout(function(){
    	var currentAddress = $modalCityInput.val();
    	if (!currentAddress.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('City can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    });
    
    $modalCityInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalPhoneNumberInput.focusout(function(){
    	var currentPhoneNumber = $modalPhoneNumberInput.val();
    	if (!currentPhoneNumber.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Phone Number can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	} else if(!isPhoneNumberValid(currentPhoneNumber)){
    		$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Phone Number format must be 999-999-9999', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    });
    
    $modalPhoneNumberInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    $modalZipCodeInput.focusout(function(){
    	var currentZipCode = $modalZipCodeInput.val();
    	if (!currentZipCode.trim()) {
        	$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Zip Code can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	} else if(!isZipcodeValid(currentZipCode)){
    		$(this).css({'background-color' : '#F6CECE'});
        	toastr.warning('Zip code format must be 99999-9999', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
    	}
    });
    
    $modalZipCodeInput.focusin(function(){
    	$(this).css({'background-color' : '#fff'});
    });
    
    function isEmailAddressValid(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    
    function isPhoneNumberValid(phoneNumber) {
        var regex = /\d{3}-\d{3}-\d{4}$/;			
        return regex.test(phoneNumber);
    }
    
    function isZipcodeValid(zipCode) {
        var regex = /\d{5}-\d{4}$|^\d{5}$/;			
        return regex.test(zipCode);
    }
    
});
