var isAuthorWroteBookGobal;
$( document ).ready(function() {
    console.log( "admin Books page ready!" );
    var apiContextPath = $("#ApiContextPathId").val();
    var restApiAuthorUrl = apiContextPath+'/authors';
    var restApiAuthorBookUrl = apiContextPath+'/authorBook/';
    var restApiNextEntityIdUrl = apiContextPath+'/nextEntityId/';
    var restApiCategoryUrl = apiContextPath+'/categories';
    var restApiPublisherUrl = apiContextPath+'/publishers'
   
   
    // ============== Author DataGrid configuration ======================
    $("#authorGridId").jsGrid({
  	  width : "100%",
  	  height :"400px",
  	  filtering : true,
  	  inserting : true,
  	  editing : true,
  	  sorting : true,
  	  paging : true,
  	  autoload : true,
	  pagerContainer: null,
	  pageIndex: 1,
	  pageSize: 10,
	  pageButtonCount: 8,
	  pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
	  pagePrevText: "Prev",
	  pageNextText: "Next",
	  pageFirstText: "First",
	  pageLastText: "Last",
	  pageNavigatorNextText: "...",
	  pageNavigatorPrevText: "...",
  	  loadMessage: "Please, wait...",
  	  deleteConfirm : "Do you really want to delete data",
  	  controller:{
  		  loadData: function(filter) {
  			  return $.ajax({
  				  type: "GET",
  				  url: restApiAuthorUrl,
  				  data: filter
  			  });
  		  	},
  		  updateItem: function(author) {
  			  return $.ajax({
  				  type: "PUT",
  				  url: restApiAuthorUrl,
  				  data: JSON.stringify(author),
  				  contentType: "application/json",
  				  dataType: "json",
  				  success : function(author){
  	    			toastr.success('author updated','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  				  	},
  				  error : function(){
  	    			toastr.error('Error updating author', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  				  	}
  			  });
  		  	},
  		  insertItem: function(author) {
  	        return $.ajax({
  	            type: "POST",
  	            url: restApiAuthorUrl,
  	            data: JSON.stringify(author),
  	            contentType: "application/json",
				dataType: "json",
				success : function(author){
	    			toastr.success('author added','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
				 },
				error : function(){
	    			toastr.error('Error adding author', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
				}
  	        });
  		  },
  		  deleteItem: function(author) {
	  		 return $.ajax({
		            type: "DELETE",
		            url: restApiAuthorUrl+"/"+author.id,
		            success : function(author){
	  	    			toastr.success('author deleted','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  				 },
	  				error : function(){
	  	    			toastr.error('Error deleting author', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  				 }
		        });		
  		  }
  	  },
  	  fields : [
  			{
  				type : "number",
  				name : "id",
  				title: "Id",
  				align: "center",
  				filtering: true,
  			    inserting: false,
  			    editing: false,
  			    sorting: true,
  			    editTemplate: function(id, author) { 
  			    	 
  				  },
  				width : 30
  				
  			},
  			{
  				type : "text",
  				name : "authorName",
  				title: "Author Name",
  				align: "center",
  				width : 175
  				
  			},
  			{
  				type : "number",
  				name : "nbrsBookWritten",
  				title: "nbrs of Book Written",
  				align: "center",
  				filtering: true,
  			    inserting: false,
  			    editing: false,
  			    sorting: true,
  			    editTemplate: function(id, author) { 
  			    	 
  				  },
  				width : 30
  				
  			},
  			{
  				type : "control"
  			}
  	  ],
  	  
  	onItemDeleting: function(args) {
  		if(args.item.nbrsBookWritten > 0){
  			toastr.error('This author has books in the system, therefore can not be deteled', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  			args.cancel = true;	
  		}else{
  			args.cancel = false;	
  		}    
    },
    
    onItemInserting: function(args) {
        if(args.item.authorName === "") {
            args.cancel = true;
            toastr.error('Author Name can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
        }else{
        	args.item.nbrsBookWritten=0;
        }
    }
});
    // ============== Category DataGrid configuration ======================
    $("#categoryTabId").click(function(){
    	$("#categoryGridId").jsGrid({
	      	  width : "100%",
	      	  height :"400px",
	      	  filtering : true,
	      	  inserting : true,
	      	  editing : true,
	      	  sorting : true,
	      	  paging : true,
	      	  autoload : true,
	  	  	  pagerContainer: null,
	  	  	  pageIndex: 1,
	  	  	  pageSize: 10,
	  	  	  pageButtonCount: 8,
	  	  	  pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
	  	  	  pagePrevText: "Prev",
	  	  	  pageNextText: "Next",
	  	  	  pageFirstText: "First",
	  	  	  pageLastText: "Last",
	  	  	  pageNavigatorNextText: "...",
	  	  	  pageNavigatorPrevText: "...",
  	    	  loadMessage: "Please, wait...",
  	    	  deleteConfirm : "Do you really want to delete data",
  	    	  controller:{
  	    		  loadData: function(filter) {
  	    			  return $.ajax({
  	    				  type: "GET",
  	    				  url: restApiCategoryUrl,
  	    				  data: filter
  	    			  });
  	    		  },
  	    		  updateItem: function(category) {
  	    			  return $.ajax({
  	    				  type: "PUT",
  	    				  url: restApiCategoryUrl,
  	    				  data: JSON.stringify(category),
  	    				  contentType: "application/json",
  	    				  dataType: "json",
  	    				  success : function(category){
  	    	    			toastr.success('category updated','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	    				  	},
  	    				  error : function(){
  	    	    			toastr.error('Error updating category', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	    				  	}
  	    			  });
  	    		  },
  	    		  insertItem: function(category) {
  	    			  return $.ajax({
  	    				  type: "POST",
  	    				  url: restApiCategoryUrl,
  	    				  data: JSON.stringify(category),
  	    				  contentType: "application/json",
  	    				  dataType: "json",
  	    				  success : function(category){
  	    					  toastr.success('category added','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	    				  },
  	    				  error : function(){
  	    					  toastr.error('Error adding category', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	    				  }
  	    			  });
  	    		  },
  	    		 deleteItem: function(category) {
	  	  	  		 return $.ajax({
	  	  		            type: "DELETE",
	  	  		            url: restApiCategoryUrl+"/"+category.id,
	  	  		            success : function(author){
	  	  	  	    			toastr.success('category deleted','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  	  	  				 },
	  	  	  				error : function(){
	  	  	  	    			toastr.error('Error deleting category', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  	  	  				 }
	  	  		     });		
  	    		  }
  	    	  },
  	    	  fields : [
  	    			{
  	    				type : "number",
  	    				name : "id",
  	    				title: "Id",
  	    				align: "center",
  	    				filtering: true,
  	    			    inserting: false,
  	    			    editing: false,
  	    			    sorting: true,
  	    			    editTemplate: function(id, author) { 
  	    			    	 
  	    				  },
  	    				width : 30
  	    				
  	    			},
  	    			{
  	    				type : "text",
  	    				name : "categoryName",
  	    				title: "Category Name",
  	    				align: "center",
  	    				width : 175
  	    				
  	    			},
  	    			{
  	    				type : "number",
  	    				name : "nbrsBooks",
  	    				title: "nbrs of Books in this Category",
  	    				align: "center",
  	    				filtering: true,
  	    			    inserting: false,
  	    			    editing: false,
  	    			    sorting: true,
  	    			    editTemplate: function(id, author) { 
  	    			    	 
  	    				  },
  	    				width : 30
  	    				
  	    			},
  	    			{
  	    				type : "control"
  	    			}
  	    	  ],
  	    	  
  	    	onItemDeleting: function(args) {
  	    		if(args.item.nbrsBooks > 0){
	  	    		toastr.error('There are books in this category, therefore can not be deteled', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  	    		args.cancel = true;	
	  	    	}else{
	  	    		args.cancel = false;	
	  	    	}    
  	    	},
  	      
  	    	onItemInserting: function(args) {
  	    		if(args.item.categoryName === "") {
	  	            args.cancel = true;
	  	            toastr.error('book category Name can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	  	        }else{
	  	          	args.item.nbrsBooks=0;
	  	       }
  	      }
      });
  });
    

  // ============== publisher DataGrid configuration ======================
  $("#publisherTabId").click(function(){
	  $("#publisherGridId").jsGrid({
	   	  width : "100%",
      	  height :"400px",
      	  filtering : true,
      	  inserting : true,
      	  editing : true,
      	  sorting : true,
      	  paging : true,
      	  autoload : true,
  	  	  pagerContainer: null,
  	  	  pageIndex: 1,
  	  	  pageSize: 10,
  	  	  pageButtonCount: 8,
  	  	  pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
  	  	  pagePrevText: "Prev",
  	  	  pageNextText: "Next",
  	  	  pageFirstText: "First",
  	  	  pageLastText: "Last",
  	  	  pageNavigatorNextText: "...",
  	  	  pageNavigatorPrevText: "...",
	    	  loadMessage: "Please, wait...",
	    	  deleteConfirm : "Do you really want to delete data",
	    	  controller:{
	    		  loadData: function(filter) {
	    			  return $.ajax({
	    				  type: "GET",
	    				  url: restApiPublisherUrl,
	    				  data: filter
	    			  });
	    		  },
	    		  updateItem: function(publisher) {
	    			  return $.ajax({
	    				  type: "PUT",
	    				  url: restApiPublisherUrl,
	    				  data: JSON.stringify(publisher),
	    				  contentType: "application/json",
	    				  dataType: "json",
	    				  success : function(publisher){
	    	    			toastr.success('publisher updated','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	    				  	},
	    				  error : function(){
	    	    			toastr.error('Error updating publisher', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	    				  	}
	    			  });
	    		  },
	    		  insertItem: function(publisher) {
	    			  console.log(publisher);
	    			  return $.ajax({
	    				  type: "POST",
	    				  url: restApiPublisherUrl,
	    				  data: JSON.stringify(publisher),
	    				  contentType: "application/json",
	    				  dataType: "json",
	    				  success : function(publisher){
	    					  toastr.success('publisher added','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	    				  },
	    				  error : function(){
	    					  toastr.error('Error adding publisher', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
	    				  }
	    			  });
	    		  },
	    		 deleteItem: function(publisher) {
  	  	  		 return $.ajax({
  	  		            type: "DELETE",
  	  		            url: restApiPublisherUrl+"/"+publisher.id,
  	  		            success : function(author){
  	  	  	    			toastr.success('publisher deleted','', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	  	  				 },
  	  	  				error : function(){
  	  	  	    			toastr.error('Error deleting publisher', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	  	  				 }
  	  		     });		
	    		  }
	    	  },
	    	  fields : [
	    			{
	    				type : "number",
	    				name : "id",
	    				title: "Id",
	    				align: "center",
	    				filtering: true,
	    			    inserting: false,
	    			    editing: false,
	    			    sorting: true,
	    			    editTemplate: function(id, author) { 
	    			    	 
	    				  },
	    				width : 30
	    				
	    			},
	    			{
	    				type : "text",
	    				name : "publisherName",
	    				title: "Publisher Name",
	    				align: "center",
	    				width : 175
	    				
	    			},
	    			{
	    				type : "number",
	    				name : "nbrsBooks",
	    				title: "nbrs of Books Published",
	    				align: "center",
	    				filtering: true,
	    			    inserting: false,
	    			    editing: false,
	    			    sorting: true,
	    			    editTemplate: function(id, author) { 
	    			    	 
	    				  },
	    				width : 30
	    				
	    			},
	    			{
	    				type : "control"
	    			}
	    	  ],
	    	  
	    	onItemDeleting: function(args) {
	    		if(args.item.nbrsBooks > 0){
  	    		toastr.error('This publisher has books in the system, can not be deteled', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	    		args.cancel = true;	
  	    	}else{
  	    		args.cancel = false;	
  	    	}    
	    	},
	      
	    	onItemInserting: function(args) {
	    		if(args.item.publisherName === "") {
  	            args.cancel = true;
  	            toastr.error('book publisher Name can not be empty', '', {timeOut: 3000, closeButton: true,positionClass: "toast-top-right"});
  	        }else{
  	          	args.item.nbrsBooks=0;
  	       }
	      }
	  });
  });
    
});
