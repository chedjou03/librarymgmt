package com.LibraryManagement.LibraryMgmt.SecurityConfig;


import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import com.LibraryManagement.LibraryMgmt.ErrorHandleling.LoggingAccessDeniedHandler;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	
	@Autowired
    private LoggingAccessDeniedHandler accessDeniedHandler;
	
	@Autowired
	DataSource datasource;

	@Autowired
	private AuthenticationSuccessHandler successHandler;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(datasource)
				.usersByUsernameQuery("select username,password, enabled from users where username=?")
				.authoritiesByUsernameQuery("select username, role from authorities where username=?")
				.passwordEncoder(passwordencoder());
	}
	
    private PasswordEncoder passwordencoder() {
		return new BCryptPasswordEncoder();
	}


	@Override
    protected void configure(HttpSecurity http) throws Exception {
        
		http.csrf().disable();//this will allow the POST , PUT and DELETE requests to come through
		http 
	       .authorizeRequests()
                    .antMatchers(
                            "/",
                            "/resource",
                            "/js/**",
                            "/css/**",
                            "/img/**",
                            "/webjars/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/**").permitAll()
                    .antMatchers(HttpMethod.POST,"/api/**").permitAll()
                    .antMatchers(HttpMethod.PUT,"**/api/**").permitAll()
                    .antMatchers(HttpMethod.DELETE,"**/api/**").permitAll()
                    .antMatchers(HttpMethod.PATCH,"**/api/**").permitAll()
                    .antMatchers("/user/**").hasRole("STUDENT")
                    .antMatchers("/librarian/**").hasRole("LIBRARIAN")
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .successHandler(successHandler)
                    .permitAll()
                .and()
                .logout()
                    .invalidateHttpSession(true)
                    .clearAuthentication(true)
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/")
                    .permitAll()
                .and()
                .exceptionHandling()
                    .accessDeniedHandler(accessDeniedHandler);
    }

 /* @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        
	   auth.inMemoryAuthentication()
                .withUser("user").password("12").roles("STUDENT")
            .and()
                .withUser("librarian").password("12").roles("LIBRARIAN")
            .and()
                .withUser("admin").password("12").roles("ADMIN");//1.5.10.RELEASE
    }*/
    
}
