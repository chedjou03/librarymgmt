package com.LibraryManagement.LibraryMgmt.SecurityConfig;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.userdetails.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.LibraryManagement.LibraryMgmt.Entity.Constant;

@Component
public class AuthenticationSuccessHandlerManager implements AuthenticationSuccessHandler{

	@Autowired 
	HttpSession theSession;
	
	@Autowired
	Constant constant;
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandlerManager.class);
	
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		String userName = getUserName(authentication);
		
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		
		String redirectUrl = getRedirectUrl(authorities);
		
		theSession.setAttribute("userName", userName);
		
		redirectStrategy.sendRedirect(request, response, redirectUrl);
	}


	private String getUserName(Authentication authentication) {
		String userName = "";
		if(authentication.getPrincipal() instanceof Principal) {
			userName = ((Principal) (authentication.getPrincipal())).getName();
		}else {
			userName = ((User)authentication.getPrincipal()).getUsername();
		}
		return userName;
	}


	private String getRedirectUrl(Collection<? extends GrantedAuthority> authorities) {
		String redirectUrl = "/";
		for (GrantedAuthority grantedAuthority : authorities) {
			if(grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
				theSession.setAttribute("userRole", grantedAuthority.getAuthority());
				redirectUrl = constant.adminRootPageUrl;
			}else if(grantedAuthority.getAuthority().equals("ROLE_LIBRARIAN")) {
				theSession.setAttribute("userRole", grantedAuthority.getAuthority());
				redirectUrl = constant.librarianRootPageUrl;
			} else if(grantedAuthority.getAuthority().equals("ROLE_STUDENT")) {
				theSession.setAttribute("userRole", grantedAuthority.getAuthority());
				redirectUrl = constant.userRootPageUrl;
			}
		}
		return redirectUrl;
	}

}
