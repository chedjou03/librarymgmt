package com.LibraryManagement.LibraryMgmt.MvcController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LibrarianController {
	
	@GetMapping("/librarian")
    public String librarianIndex() {
        return "librarian/librarianIndex";
    }
    
}
