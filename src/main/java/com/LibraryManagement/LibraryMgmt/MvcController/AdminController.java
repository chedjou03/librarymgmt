package com.LibraryManagement.LibraryMgmt.MvcController;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.LibraryManagement.LibraryMgmt.Entity.Constant;



@Controller
public class AdminController {
	
	@Autowired 
	HttpSession theSession;
	
	@Autowired
	Constant constant;
	
	@GetMapping("/admin")
    public String adminIndex() {
        return "admin/adminIndex";
    }
	
	@GetMapping("/admin/adminProfile")
    public String adminProfile(Model theModel) {
		theModel.addAttribute("userName", theSession.getAttribute("userName"));
		theModel.addAttribute("userRole", theSession.getAttribute("userRole"));
		theModel.addAttribute("apiContextPath", constant.localApiApplicationContextPath);
		
        return "admin/adminProfile";
    }
	
	@GetMapping("/admin/adminBooks")
    public String adminBooks(Model theModel) {
		theModel.addAttribute("userName", theSession.getAttribute("userName"));
		theModel.addAttribute("userRole", theSession.getAttribute("userRole"));
		theModel.addAttribute("apiContextPath", constant.localApiApplicationContextPath);
		
        return "admin/adminBooks";
    }
	
	@GetMapping("/admin/adminUsers")
    public String adminUsers(Model theModel) {
		theModel.addAttribute("userName", theSession.getAttribute("userName"));
		theModel.addAttribute("userRole", theSession.getAttribute("userRole"));
		theModel.addAttribute("apiContextPath", constant.localApiApplicationContextPath);
		
        return "admin/adminUsers";
    }
	
    
}
