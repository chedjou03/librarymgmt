package com.LibraryManagement.LibraryMgmt.MvcController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String root() {
    	
        return "index";
    }
   
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    
    @GetMapping("/resource")
    public String resource() {
        return "resource";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }
    
}
