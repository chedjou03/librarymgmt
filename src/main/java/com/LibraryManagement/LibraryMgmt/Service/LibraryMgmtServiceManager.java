package com.LibraryManagement.LibraryMgmt.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.LibraryManagement.LibraryMgmt.DAO.LibraryMgmtDao;
import com.LibraryManagement.LibraryMgmt.Entity.Author;
import com.LibraryManagement.LibraryMgmt.Entity.Category;
import com.LibraryManagement.LibraryMgmt.Entity.Employee;
import com.LibraryManagement.LibraryMgmt.Entity.Publisher;
import com.LibraryManagement.LibraryMgmt.Entity.SystemUser;

@Service
@Transactional
public class LibraryMgmtServiceManager implements LibraryMgmtService {

	@Autowired
	private LibraryMgmtDao libraryMgmtDao;
	
	@Override
	public List<Employee> getEmployees() {
		return libraryMgmtDao.getEmployees();
	}

	@Override
	public Employee getEmployeeByUserName(String empUserName) {
		
		return libraryMgmtDao.getEmployeeByUserName(empUserName) ;
	}

	@Override
	public void updateEmployee(Employee theEmployee) {
		libraryMgmtDao.updateEmployee(theEmployee);
		
	}

	@Override
	public List<Category> getCategories() {
		return libraryMgmtDao.getCategories();
	}

	@Override
	public Category updateCategory(Category theCategory) {
		return libraryMgmtDao.updateCategory(theCategory);
	}

	@Override
	public List<Author> getAuthors() {
		return libraryMgmtDao.getAuthors();
	}

	@Override
	public Author getAuthor(Integer authorId) {
		return libraryMgmtDao.getAuthor(authorId);
	}

	@Override
	public Integer getNextEntityId(String entityName) {
		return libraryMgmtDao.getNextEntityId(entityName);
	}

	@Override
	public Author updateAuthor(Author theAuthor) {
		return libraryMgmtDao.updateAuthor(theAuthor);
	}

	@Override
	public Integer getIsAuthorWroteBook(Integer authorId) {
		return libraryMgmtDao.getIsAuthorWroteBook(authorId);
	}

	@Override
	public Integer deleteAuthor(Integer theAuthorId) {
		return libraryMgmtDao.deleteAuthor(theAuthorId);
	}

	@Override
	public Author addAuthor(Author theAuthor) {
		return libraryMgmtDao.addAuthor(theAuthor);
	}

	@Override
	public Integer deleteCategory(Integer theCategoryId) {
		return libraryMgmtDao.deleteCategory(theCategoryId);
	}

	@Override
	public Category addCategory(Category theCategory) {
		return libraryMgmtDao.addCategory(theCategory);
	}

	@Override
	public List<Publisher> getPublisher() {
		return libraryMgmtDao.getPublisher();
	}

	@Override
	public Integer deletePublisher(Integer thePublisherId) {
		return libraryMgmtDao.deletePublisher(thePublisherId);
	}

	@Override
	public Publisher updatePublisher(Publisher thePublisher) {
		return libraryMgmtDao.updatePublisher(thePublisher);
	}

	@Override
	public Publisher addPublisher(Publisher thePublisher) {
		return libraryMgmtDao.addPublisher(thePublisher);
	}

	@Override
	public List<SystemUser> getLibrarians() {
		return libraryMgmtDao.getLibrarians();
	}

}
