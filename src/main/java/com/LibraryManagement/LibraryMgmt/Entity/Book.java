package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "book")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id")
	private Integer id;
	
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "title")
	private String title;
	
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "author_id")
	@JsonIgnore
	private Author author;
	
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "publisher_id")
	//@JsonIgnore
	private Publisher publisher;
	
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "category_id")
	//@JsonIgnore
	private Category category;
	
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "added_date")
	private Date addedDate;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	
	@Column(name = "publication_date")
	private Date publicationDate;
	
	@Column(name = "language")
	private String language;
	

	@Column(name = "nbr_pages")
	private Integer nbrPages;

	public Book(Integer id, String isbn, String title, Author author, Publisher publisher, Category category,
			Integer quantity, Date addedDate, Date lastUpdate, Date publicationDate, String language,
			Integer nbrPages) {
		super();
		this.id = id;
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.category = category;
		this.quantity = quantity;
		this.addedDate = addedDate;
		this.lastUpdate = lastUpdate;
		this.publicationDate = publicationDate;
		this.language = language;
		this.nbrPages = nbrPages;
	}

	public Book() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	
	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getNbrPages() {
		return nbrPages;
	}

	public void setNbrPages(Integer nbrPages) {
		this.nbrPages = nbrPages;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", isbn=" + isbn + ", title=" + title + ", author=" + author + ", publisher="
				+ publisher + ", category=" + category + ", quantity=" + quantity + ", addedDate=" + addedDate
				+ ", lastUpdate=" + lastUpdate + ", publicationDate=" + publicationDate + ", language=" + language
				+ ", nbrPages=" + nbrPages + "]";
	}

}
