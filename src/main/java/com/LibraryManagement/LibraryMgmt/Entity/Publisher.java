package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "publisher")
public class Publisher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "publisher_id")
	private Integer id;
	
	@Column(name = "publisher_name")
	private String publisherName;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	@Column(name = "nbr_books")
	private Integer nbrsBooks;
	
	@OneToMany(	mappedBy = "publisher",
		    fetch = FetchType.LAZY,
			cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}
		  )
	@JsonIgnore
	private List<Book> books;

	public Publisher(Integer id, String publisherName, Date lastUpdate, Integer nbrsBooks, List<Book> books) {
		super();
		this.id = id;
		this.publisherName = publisherName;
		this.lastUpdate = lastUpdate;
		this.nbrsBooks = nbrsBooks;
		this.books = books;
	}


	public Publisher() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	public Integer getNbrsBooks() {
		return nbrsBooks;
	}

	public void setNbrsBooks(Integer nbrsBooks) {
		this.nbrsBooks = nbrsBooks;
	}

	@Override
	public String toString() {
		return "Publisher [id=" + id + ", publisherName=" + publisherName + ", lastUpdate=" + lastUpdate + ", nbr of books published="
				+ books.size() + "]";
	}


}
