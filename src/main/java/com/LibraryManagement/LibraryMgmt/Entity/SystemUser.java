package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.Date;

public class SystemUser extends Employee{
	
	private String role;
	
	private String password;
	
	private Boolean enabled;
	
	private Integer addressId;
	
	private String userName;
	

	public SystemUser(String firstName, String middleName, String lastName, String email, Address address, User user,
			Date addedDate, Date lastUpdate, String role, String password, Boolean enabled, Integer addressId,
			String userName) {
		super(firstName, middleName, lastName, email, address, user, addedDate, lastUpdate);
		this.role = role;
		this.password = password;
		this.enabled = enabled;
		this.addressId = addressId;
		this.userName = userName;
	}

	public SystemUser(String firstName, String middleName, String lastName, String email, Address address, User user,
			Date addedDate, Date lastUpdate) {
		super(firstName, middleName, lastName, email, address, user, addedDate, lastUpdate);
	}
	
	public SystemUser() {
		super();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "SystemUser [role=" + role + ", password=" + password + ", enabled=" + enabled + ", addressId="
				+ addressId + ", userName=" + userName + ", getId()=" + getId() + ", getFirstName()=" + getFirstName()
				+ ", getMiddleName()=" + getMiddleName() + ", getLastName()=" + getLastName() + ", getEmail()="
				+ getEmail() + ", getAddress()=" + getAddress() + ", getUser()=" + getUser() + ", getAddedDate()="
				+ getAddedDate() + ", getLastUpdate()=" + getLastUpdate() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}


}
