package com.LibraryManagement.LibraryMgmt.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "authorities")
public class Authority {
	
	@Id
	@Column(name = "username")
	private String username;
	
	@Column(name = "role")
	private String role;

	public Authority(String username, String role) {
		super();
		this.username = username;
		this.role = role;
	}

	public Authority() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Authority [username=" + username + ", role=" + role + "]";
	}

}
