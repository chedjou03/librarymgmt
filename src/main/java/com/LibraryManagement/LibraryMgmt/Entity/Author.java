package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "author")
public class Author {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "author_id")
	private Integer id;
	
	@Column(name = "author_name")
	private String authorName;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	
	@OneToMany(	mappedBy = "author",
		    fetch = FetchType.LAZY,
			cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH}
		  )
	//@JsonIgnore
	private List<Book> books;
	
	@Column(name = "nbr_books")
	private Integer nbrsBookWritten;

	public Author() {
		super();
	}

	public Author(Integer id, String authorName, Date lastUpdate, List<Book> books, Integer nbrsBookWritten) {
		super();
		this.id = id;
		this.authorName = authorName;
		this.lastUpdate = lastUpdate;
		this.books = books;
		this.nbrsBookWritten = nbrsBookWritten;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}	

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Integer getNbrsBookWritten() {
		return nbrsBookWritten;
	}

	public void setNbrsBookWritten(Integer nbrsBookWritten) {
		this.nbrsBookWritten = nbrsBookWritten;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", authorName=" + authorName + ", lastUpdate=" + lastUpdate + ", nbrs of books written=" + books.size()
				+ "]";
	}

}
