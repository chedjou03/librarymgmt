package com.LibraryManagement.LibraryMgmt.Entity;

import org.springframework.stereotype.Component;

@Component
public class SqlHelper {
	
	public static final String listLibarian = "\n" + 
			"select employee.employee_id,employee.first_name, employee.middle_name, employee.last_name, employee.email, employee.address_id,  employee.username,employee.added_date,employee.last_update authorities,role, users.password, users.enabled\n" + 
			" from employee , authorities  , users  \n" + 
			"where employee.username = authorities.username and  employee.username = users.username and authorities.role =  'ROLE_LIBRARIAN'";

}
