package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "category")
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private int id;
	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	@OneToMany(	mappedBy = "category",
		    fetch = FetchType.LAZY,
			cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}
		  )
	@JsonIgnore
	private List<Book> books;
	
	@Column(name = "nbr_books")
	private Integer nbrsBooks;

	public Category(int id, String categoryName, Date lastUpdate, List<Book> books, Integer nbrsBooks) {
		super();
		this.id = id;
		this.categoryName = categoryName;
		this.lastUpdate = lastUpdate;
		this.books = books;
		this.nbrsBooks = nbrsBooks;
	}

	public Category() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Integer getNbrsBooks() {
		return nbrsBooks;
	}

	public void setNbrsBooks(Integer nbrsBooks) {
		this.nbrsBooks = nbrsBooks;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", categoryName=" + categoryName + ", lastUpdate=" + lastUpdate + ", nbs of books in this category="
				+ books.size() + "]";
	}



}
