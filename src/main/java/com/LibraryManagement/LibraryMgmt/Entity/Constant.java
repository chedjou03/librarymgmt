package com.LibraryManagement.LibraryMgmt.Entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Constant {
	
	public  final String adminRootPageUrl = "/admin";
	
	public  final String librarianRootPageUrl = "/librarian";
	
	public  final String userRootPageUrl = "/student";
	
	public final String localApiApplicationContextPath = "http://localhost:8080/api";
	
	private  String[] states = {"Alabama", "Arkansas", "Arizona", "Alaska", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming" };
	
	public List<State> getStates(){
		List<State> listStates = new ArrayList<>();
		for (int i = 0; i < states.length ; i++) {
			listStates.add(new State(i,states[i]));
		}
		return listStates;
	}

}
