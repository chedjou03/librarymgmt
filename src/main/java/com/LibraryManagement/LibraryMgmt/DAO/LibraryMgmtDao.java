package com.LibraryManagement.LibraryMgmt.DAO;

import java.util.List;

import com.LibraryManagement.LibraryMgmt.Entity.Author;
import com.LibraryManagement.LibraryMgmt.Entity.Category;
import com.LibraryManagement.LibraryMgmt.Entity.Employee;
import com.LibraryManagement.LibraryMgmt.Entity.Publisher;
import com.LibraryManagement.LibraryMgmt.Entity.SystemUser;

public interface LibraryMgmtDao {
	
	public List<Employee> getEmployees();

	public Employee getEmployeeByUserName(String empUserName);

	public void updateEmployee(Employee theEmployee);

	public List<Category> getCategories();

	public Category updateCategory(Category theCategory);

	public List<Author> getAuthors();

	public Author getAuthor(Integer authorId);

	public Integer getNextEntityId(String entityName);

	public Author updateAuthor(Author theAuthor);

	public Integer getIsAuthorWroteBook(Integer authorId);

	public Integer deleteAuthor(Integer theAuthorId);

	public Author addAuthor(Author theAuthor);

	public Integer deleteCategory(Integer theCategoryId);

	public Category addCategory(Category theCategory);

	public List<Publisher> getPublisher();

	public Integer deletePublisher(Integer thePublisherId);

	public Publisher updatePublisher(Publisher thePublisher);

	public Publisher addPublisher(Publisher thePublisher);

	public List<SystemUser> getLibrarians();

}
