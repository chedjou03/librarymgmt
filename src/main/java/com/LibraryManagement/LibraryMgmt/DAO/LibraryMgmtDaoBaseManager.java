package com.LibraryManagement.LibraryMgmt.DAO;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.LibraryManagement.LibraryMgmt.Entity.Author;
import com.LibraryManagement.LibraryMgmt.Entity.Employee;



public class LibraryMgmtDaoBaseManager {
	
	@Autowired
	protected EntityManager entityManager;
	
	private static final Logger logger = LoggerFactory.getLogger(LibraryMgmtDaoBaseManager.class);
	
	
	public <T> List<T> getListEntity(String EntityName, String orderColumn){
		logger.info("get List of all "+EntityName);
		Query theQuery = entityManager.createQuery("from "+EntityName+" order by "+orderColumn);
		List<T> listEntity = theQuery.getResultList();
		return listEntity;	
	}
	
	public Employee getEmployeeByUserName(String empUserName) {
		logger.info("get Employee with Username: "+empUserName);
		Query theQuery = entityManager.createQuery("from Employee where userName = '"+empUserName+"'");
		Employee theEmployee = (Employee) theQuery.getSingleResult();
		return theEmployee;
		
	}
	
	public <T> void  updateEntity (T theEntity) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theEntity);
	}
	
	public <T> void  saveEntity (T theEntity) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theEntity);
	}
	
	public <T> T  getEntity (String entityName, Integer theEntityId) {
		Query theQuery = entityManager.createQuery("from "+entityName+" where id = "+theEntityId);
		T theEntity = (T)theQuery.getSingleResult();
		return theEntity;
	}

	public Integer getNextEntityId(String entityName) {
		Query theQuery = entityManager.createQuery("select max(id) from "+entityName);
		Integer nextEntityId = (Integer)theQuery.getSingleResult() + 1;
		return nextEntityId;
	}

	public Integer getIsAuthorWroteBook(Author theAuthor) {
		Query theQuery = entityManager.createQuery("select count(*) from Book where Author= "+theAuthor);
		Integer nbrBooksWrittenByAuthor = (Integer)theQuery.getSingleResult();
		return new Integer (nbrBooksWrittenByAuthor);
	}

	public Integer deleteEntity(String entityName, Integer theEntityId) {
		Query theQuery = entityManager.createQuery("delete from "+entityName+" where id = "+theEntityId);
		return new Integer(theQuery.executeUpdate());
	}

}
