package com.LibraryManagement.LibraryMgmt.DAO;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.LibraryManagement.LibraryMgmt.Entity.Address;
import com.LibraryManagement.LibraryMgmt.Entity.Author;
import com.LibraryManagement.LibraryMgmt.Entity.Category;
import com.LibraryManagement.LibraryMgmt.Entity.Employee;
import com.LibraryManagement.LibraryMgmt.Entity.Publisher;
import com.LibraryManagement.LibraryMgmt.Entity.SqlHelper;
import com.LibraryManagement.LibraryMgmt.Entity.SystemUser;


@Repository
public class LibraryMgmtDaoManager extends LibraryMgmtDaoBaseManager implements LibraryMgmtDao {

	private static final Logger logger = LoggerFactory.getLogger(LibraryMgmtDaoManager.class);
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private SqlHelper sqlHelper;
	
	
	
	@Override
	public List<Employee> getEmployees() {
		List<Employee> employees = getListEntity(Employee.class.getSimpleName(), "firstName");		
		return employees;
	}

	public Employee getEmployeeByUserName(String empUserName) {
		Employee theEmployee = super.getEmployeeByUserName(empUserName);
		//passwordCheck(theEmployee.getUser().getPassword());
		return theEmployee;
	}

	private void passwordCheck(String password) {
		System.out.println("Passowrd "+password);
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		if(encoder.matches(password, "123")) {
		   System.out.println("Passowrd matches");
		}else{
		   System.out.println("Passowrd dont match matches");
		}
		
	}

	@Override
	public void updateEmployee(Employee theEmployee) {
		theEmployee.setLastUpdate(new Date());
		theEmployee.getAddress().setLastUpdate(new Date());
		super.updateEntity(theEmployee);
	}
	
	
	public void saveEmployee (Employee theEmployee) {
		super.saveEntity(theEmployee);
	}

	@Override
	public List<Category> getCategories() {
		List<Category> categories = getListEntity(Category.class.getSimpleName(), "categoryName");		
		return categories;
	}

	@Override
	public Category updateCategory(Category theCategory) {
		theCategory.setLastUpdate(new Date());
		super.updateEntity(theCategory);
		return theCategory;
	}

	@Override
	public List<Author> getAuthors() {
		List<Author> authors = getListEntity(Author.class.getSimpleName(), "id");		
		return authors;
	}

	@Override
	public Author getAuthor(Integer authorId) {
		return getEntity(Author.class.getSimpleName(),authorId);
	}
	
	
	public Address getAddress(Integer addressId) {
		return getEntity(Address.class.getSimpleName(),addressId);
	}
	
	

	@Override
	public Integer getNextEntityId(String entityName) {
		
		if(entityName.equalsIgnoreCase(Author.class.getSimpleName())) {
			return super.getNextEntityId(Author.class.getSimpleName());
		}else if(entityName.equalsIgnoreCase(Publisher.class.getSimpleName())) {
			return super.getNextEntityId(Publisher.class.getSimpleName());
		}if(entityName.equalsIgnoreCase(Employee.class.getSimpleName())) {
			return super.getNextEntityId(Employee.class.getSimpleName());
		}else {
			return 100;
		}
		
	}

	@Override
	public Author updateAuthor(Author theAuthor) {
		theAuthor.setLastUpdate(new Date());
		super.updateEntity(theAuthor);
		return theAuthor;
	}

	@Override
	public Integer getIsAuthorWroteBook(Integer authorId) {
		Author theAuthor = super.getEntity("Author",authorId);
		if(theAuthor.getBooks().size() >= 1) {
			return new Integer(1);
		}else {
			return new Integer(0);
		}
	}

	@Override
	public Integer deleteAuthor(Integer theAuthorId) {
		return super.deleteEntity(Author.class.getSimpleName(),theAuthorId);
	}

	@Override
	public Author addAuthor(Author theAuthor) {
		theAuthor.setLastUpdate(new Date());
		super.saveEntity(theAuthor);
		return theAuthor;
	}

	@Override
	public Integer deleteCategory(Integer theCategoryId) {
		return super.deleteEntity(Category.class.getSimpleName(),theCategoryId);
	}

	@Override
	public Category addCategory(Category theCategory) {
		theCategory.setLastUpdate(new Date());
		super.saveEntity(theCategory);
		return theCategory;
	}

	@Override
	public List<Publisher> getPublisher() {
		List<Publisher> publishers = getListEntity(Publisher.class.getSimpleName(), "id");		
		return publishers;
	}

	@Override
	public Integer deletePublisher(Integer thePublisherId) {
		return super.deleteEntity(Publisher.class.getSimpleName(),thePublisherId);
	}

	@Override
	public Publisher updatePublisher(Publisher thePublisher) {
		thePublisher.setLastUpdate(new Date());
		super.updateEntity(thePublisher);
		return thePublisher;
	}

	@Override
	public Publisher addPublisher(Publisher thePublisher) {
		thePublisher.setLastUpdate(new Date());
		super.saveEntity(thePublisher);
		return thePublisher;
	}

	@Override
	public List<SystemUser> getLibrarians() {
		Query theQuery  = entityManager.createNativeQuery(SqlHelper.listLibarian);
		List<Object[]> rows = theQuery.getResultList();
		List<SystemUser> libarians = new ArrayList<SystemUser>();
		
		for(Object[] row : rows){
			SystemUser tempLibarian = new SystemUser();
			tempLibarian.setId(Integer.parseInt(row[0].toString()));
			tempLibarian.setFirstName(row[1].toString());
			tempLibarian.setMiddleName(row[2].toString());
			tempLibarian.setLastName(row[3].toString());
			tempLibarian.setEmail(row[4].toString());
			tempLibarian.setAddressId(Integer.parseInt(row[5].toString()));
			tempLibarian.setUserName(row[6].toString());
			tempLibarian.setAddedDate(convertMysqlDateToJavaDate(row[7].toString()));
			tempLibarian.setLastUpdate(convertMysqlDateToJavaDate(row[8].toString()));
			tempLibarian.setRole(row[9].toString());
			tempLibarian.setPassword(row[10].toString());
			tempLibarian.setEnabled(new Boolean(row[11].toString()));
			tempLibarian.setAddress(getAddress(tempLibarian.getAddressId()));
			libarians.add(tempLibarian);
		}
		return libarians;
	}

	private Date convertMysqlDateToJavaDate(String mysqlDate)  {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = new Date();
		try {
			date = format.parse(mysqlDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


}
