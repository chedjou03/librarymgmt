package com.LibraryManagement.LibraryMgmt.RestAPIController;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.LibraryManagement.LibraryMgmt.Entity.Author;
import com.LibraryManagement.LibraryMgmt.Entity.Category;
import com.LibraryManagement.LibraryMgmt.Entity.Constant;
import com.LibraryManagement.LibraryMgmt.Entity.Employee;
import com.LibraryManagement.LibraryMgmt.Entity.Publisher;
import com.LibraryManagement.LibraryMgmt.Entity.State;
import com.LibraryManagement.LibraryMgmt.Entity.SystemUser;
import com.LibraryManagement.LibraryMgmt.Service.LibraryMgmtService;


@RestController
@RequestMapping("/api")
public class LibraryMgmtRestApiController {
	
	@Autowired
	private LibraryMgmtService libraryMgmtService;
	
	@Autowired
	private Constant constant;
	
	@GetMapping("/employees")
	public List<Employee> getEmployees() {
		return libraryMgmtService.getEmployees();
	}
	
	@GetMapping("/employees/{empUserName}")
	public Employee getEmployeeByUserName(@PathVariable String empUserName) {
		return libraryMgmtService.getEmployeeByUserName(empUserName);
	}
	
	
	@PutMapping("/employees")
	public Employee updateEmployee(@RequestBody Employee theEmployee)
	{	
		libraryMgmtService.updateEmployee(theEmployee);
		return theEmployee;
	}
	
	@PostMapping("/states")
	public State createState(@RequestBody State theState)
	{	
		return theState;
	}
	
	@GetMapping("/states")
	public List<State> getStates(){
		return constant.getStates();
	}
	
	@GetMapping("/categories")
	public List<Category> getCategories(){
		List<Category> categories = libraryMgmtService.getCategories();
		if(categories != null) {
			for(Category tempCategory : categories) {
				tempCategory.setNbrsBooks(tempCategory.getBooks().size());
			}
		}
		return  categories;	
	}
	
	@PutMapping("/categories")
	public Category updateCategory(@RequestBody Category theCategory) {
		return  libraryMgmtService.updateCategory(theCategory);
	}
	
	@DeleteMapping("/categories/{theCategoryId}")
	public Integer deleteCategory(@PathVariable Integer theCategoryId)
	{
		return libraryMgmtService.deleteCategory(theCategoryId);
	}
	
	@PostMapping("/categories")
	public Category addCategory(@RequestBody Category theCategory)
	{	
		return libraryMgmtService.addCategory(theCategory);
	}
	
	@GetMapping("/authors")
	public List<Author> getAuthors(){
		List<Author> authors = libraryMgmtService.getAuthors();
		if(authors != null) {
			for(Author tempAuthor : authors) {
				tempAuthor.setNbrsBookWritten(tempAuthor.getBooks().size());
			}
		}
		return  authors;	
	}
	
	@PostMapping("/authors")
	public Author addAuthor(@RequestBody Author theAuthor)
	{	
		return libraryMgmtService.addAuthor(theAuthor);
	}
	
	@PutMapping("/authors")
	public Author updateAuthor(@RequestBody Author theAuthor) {
		return  libraryMgmtService.updateAuthor(theAuthor);
	}
	
	@GetMapping("/authors/{authorId}")
	public Author getAuthor(@PathVariable Integer authorId) {
		Author theAuthor = libraryMgmtService.getAuthor(authorId);
		theAuthor.setNbrsBookWritten(theAuthor.getBooks().size());
		return theAuthor;
	}
	
	@GetMapping("/authors?id={authorId}&authorName={authorName}&nbrsBookWritten={nbrsBookWritten}")
	public List<Author> searchAuthor(@PathVariable Integer authorId,@PathVariable String authorName, @PathVariable Integer nbrsBookWritten) {
		System.out.println(">>>>>>>>>>> u made it here");
		return libraryMgmtService.getAuthors();
	}
	
	@DeleteMapping("/authors/{theAuthorId}")
	public Integer deleteAuthor(@PathVariable Integer theAuthorId)
	{
		return libraryMgmtService.deleteAuthor(theAuthorId);
	}
	
	
	@GetMapping("/authorBook/{authorId}")
	public Integer getIsAuthorWroteBook(@PathVariable Integer authorId) {
		return libraryMgmtService.getIsAuthorWroteBook(authorId);
	}
	
	@GetMapping("/nextEntityId/{entityName}")
	public Integer getNextEntityId(@PathVariable String entityName){
		return  libraryMgmtService.getNextEntityId(entityName);	
	}
	
	
	@GetMapping("/publishers")
	public List<Publisher> getPublishers(){
		List<Publisher> publishers = libraryMgmtService.getPublisher();
		if(publishers != null) {
			for(Publisher tempPublisher : publishers) {
				tempPublisher.setNbrsBooks(tempPublisher.getBooks().size());
			}
		}
		return  publishers;	
	}
	
	@DeleteMapping("/publishers/{thePublisherId}")
	public Integer deletePublisher(@PathVariable Integer thePublisherId)
	{
		return libraryMgmtService.deletePublisher(thePublisherId);
	}
	
	@PutMapping("/publishers")
	public Publisher updatePublisher(@RequestBody Publisher thePublisher) {
		return  libraryMgmtService.updatePublisher(thePublisher);
	}
	
	@PostMapping("/publishers")
	public Publisher addPublisher(@RequestBody Publisher thePublisher)
	{	
		return libraryMgmtService.addPublisher(thePublisher);
	}
	
	@GetMapping("/librarians")
	public List<SystemUser> getLibrarians(){
		List<SystemUser> librarians = libraryMgmtService.getLibrarians();
		return  librarians;	
	}
	
	
}
